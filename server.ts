import path from "path";
import cron from "node-cron";

import ElasticsearchService from "./controller/ElasticsearchService";
import DataExtractor from "./controller/DataExtractor";
import CSVExporter from "./controller/CsvExporter";
import { transferIntraBankv3 } from "./data/QueryTransferIntraBankV3";

const apiUrl = "http://127.0.0.1:9200";

const elasticsearchService = new ElasticsearchService(apiUrl);

cron.schedule("* * * * *", () => {
  elasticsearchService
    .fetchData(transferIntraBankv3)
    .then((data) => {
      const extractor1 = new DataExtractor("$log.INCOMING_RESPONSE");
      const exporter1 = new CSVExporter(
        ["partnerReferenceNo", "transactionId", "channel", "responseMessage"],
        extractor1.extractData(data),
        path.join(__dirname, "export", "output1.csv")
      );
      exporter1.exportToCSV();
    })
    .catch((error) => {
      console.error("Error:", error);
    });
});
