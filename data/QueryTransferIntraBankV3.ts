export const transferIntraBankv3 = {
  _source: ["$log.INCOMING_RESPONSE"],
  query: {
    bool: {
      must: [],
      filter: [
        {
          bool: {
            should: [
              {
                match_phrase: {
                  "$httpRequest.requestURL": "/transferIntraBankv3",
                },
              },
            ],
            minimum_should_match: 1,
          },
        },
        {
          range: {
            "@timestamp": {
              format: "strict_date_optional_time",
              gte: "2023-09-30T17:00:00.000Z",
              lte: "2023-10-07T16:59:59.999Z",
            },
          },
        },
      ],
      should: [],
      must_not: [],
    },
  },
};
