class DataExtractor {
  private dataKey: string;

  constructor(dataKey: string) {
    this.dataKey = dataKey;
  }

  public extractData(data: any): any[] {
    return data.hits.hits.map((hit: any) => {
      const response = hit._source[this.dataKey];
      const responseData = JSON.parse(response);
      return responseData;
    });
  }
}

export default DataExtractor;
