class ElasticsearchService {
  private apiUrl: string;

  constructor(apiUrl: string) {
    this.apiUrl = apiUrl;
  }

  public async fetchData(searchQuery: any): Promise<any> {
    const response = await fetch(`${this.apiUrl}/bsi-logs*/_search`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(searchQuery),
    });

    if (!response.ok) {
      throw new Error(`Failed to fetch data. Status: ${response.status}`);
    }

    return response.json();
  }
}

export default ElasticsearchService;
