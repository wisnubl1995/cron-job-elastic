import fs from "fs";
import path from "path";

class CSVExporter {
  private headers: string[];
  private data: any[];
  private filePath: string;

  constructor(headers: string[], data: any[], filePath: string) {
    this.headers = headers;
    this.data = data;
    this.filePath = filePath;
  }

  public exportToCSV(): void {
    const csvData = this.data.map((entry: any) => {
      return this.headers.map((header) => entry[header]).join(",");
    });

    const csvContent = this.headers.join(",") + "\n" + csvData.join("\n");

    fs.mkdirSync(path.dirname(this.filePath), { recursive: true });

    fs.writeFileSync(this.filePath, csvContent);

    console.log(`CSV file saved at: ${this.filePath}`);
  }
}

export default CSVExporter;
